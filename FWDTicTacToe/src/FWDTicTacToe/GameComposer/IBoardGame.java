/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FWDTicTacToe.GameComposer;

import FWDTicTacToe.GamePlayers.IPlayer;
import FWDTicTacToe.GameRules.IBoardGameRule;
import java.util.List;

/**
 *
 * @author Vegi
 * @param <TGameIdentifier>
 * @param <TGameResult>
 * @param <TGameStatus>
 */
public interface IBoardGame<TGameIdentifier, TGameResult, TGameStatus> {
    boolean SetGameRule(IBoardGameRule<TGameIdentifier, TGameResult> rule);
    IBoardGameRule<TGameIdentifier, TGameResult> GetGameRule();
    List<IPlayer<TGameIdentifier, TGameResult>> GetPlayers();
    boolean Start();
    boolean Pause();
    TGameStatus GetGameStatus();
    void UpdateGame(TGameIdentifier id, int currentRow, int currentCol);
}
