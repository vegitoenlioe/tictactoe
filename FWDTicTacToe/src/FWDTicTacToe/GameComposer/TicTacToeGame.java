/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FWDTicTacToe.GameComposer;

import FWDTicTacToe.Enums.GameResult;
import FWDTicTacToe.Enums.GameStatus;
import FWDTicTacToe.Enums.TicTacToeIdentifier;
import FWDTicTacToe.GamePlayers.IPlayer;
import FWDTicTacToe.GamePlayers.TicTacToePlayer;
import FWDTicTacToe.GameRules.IBoardGameRule;
import FWDTicTacToe.GameRules.TicTacToeGameRule;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Vegi
 */
public class TicTacToeGame implements IBoardGame<TicTacToeIdentifier, GameResult, GameStatus> {
    private IBoardGameRule<TicTacToeIdentifier, GameResult> _rule;
    private final List<IPlayer<TicTacToeIdentifier, GameResult>> _players;
    private GameStatus _status;
    
    public TicTacToeGame(int boardRowCount) {
        _rule = new TicTacToeGameRule(boardRowCount);
        _players = GeneratePlayers();
        _status = GameStatus.NONE;
    }
    
    private List<IPlayer<TicTacToeIdentifier, GameResult>> GeneratePlayers() {
        List<IPlayer<TicTacToeIdentifier, GameResult>> result = new ArrayList<>();
        
        int playerCount = _rule.GetPlayerNum();
        List<TicTacToeIdentifier> ids = new ArrayList<>();
        ids.add(TicTacToeIdentifier.CROSS);
        ids.add(TicTacToeIdentifier.NOUGHT);
        
        while (result.size() < playerCount) {
            Integer playerIdx = result.size() + 1;
            TicTacToePlayer player = new TicTacToePlayer(playerIdx.toString());
            
            if(ids.size() > 0) {
                player.SetIdentifier(ids.get(0));
                ids.remove(ids.get(0));
            }
            
            result.add(player);
        }
        
        return result;
    }
    
    @Override
    public boolean SetGameRule(IBoardGameRule<TicTacToeIdentifier, GameResult> rule) {
        _rule = rule;
        
        return true;
    }
    
    @Override
    public IBoardGameRule<TicTacToeIdentifier, GameResult> GetGameRule() {
        return _rule;
    }

    @Override
    public List<IPlayer<TicTacToeIdentifier, GameResult>> GetPlayers() {
        return _players;
    }

    @Override
    public boolean Start() {
        if(_status.equals(GameStatus.STARTED))
            return false;
        
        _status = GameStatus.STARTED;
        return true;
    }

    @Override
    public boolean Pause() {
        if(!_status.equals(GameStatus.STARTED))
            return false;
        
        _status = GameStatus.PAUSED;
        return true;
    }

    @Override
    public GameStatus GetGameStatus() {
        return _status;
    }
    
    @Override
    public void UpdateGame(TicTacToeIdentifier id, int currentRow, int currentCol) {
        if (_rule.GetGameResult(id, currentRow, currentCol).equals(GameResult.WIN)) {
            _players.forEach((player) -> {
                if(player.GetIdentifier().equals(id))
                    player.SetGameResult(GameResult.WIN);
                else
                    player.SetGameResult(GameResult.LOSE);
            });
            _status = GameStatus.FINISHED;
        } else if (_rule.GetGameResult(id, currentRow, currentCol).equals(GameResult.DRAW)) {  // check for draw
            _players.forEach((player) -> {
                player.SetGameResult(GameResult.DRAW);
            });
            _status = GameStatus.FINISHED;
        }
    }
}
