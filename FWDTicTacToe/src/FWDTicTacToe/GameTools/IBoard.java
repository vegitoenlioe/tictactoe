/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FWDTicTacToe.GameTools;

/**
 *
 * @author Vegi
 * @param <TBoardIdentifier>
 */
public interface IBoard<TBoardIdentifier> {
    TBoardIdentifier[][] GetBoardDetail();
}
