/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FWDTicTacToe.GameTools;

import FWDTicTacToe.Enums.TicTacToeIdentifier;

/**
 *
 * @author Vegi
 */
public class TicTacToeBoard implements IBoard<TicTacToeIdentifier> {
    private final TicTacToeIdentifier[][] _boardDetail;
    
    public TicTacToeBoard(int rowCount) {
        _boardDetail = new TicTacToeIdentifier[rowCount][rowCount];
    }

    @Override
    public TicTacToeIdentifier[][] GetBoardDetail() {
        return _boardDetail;
    }
}