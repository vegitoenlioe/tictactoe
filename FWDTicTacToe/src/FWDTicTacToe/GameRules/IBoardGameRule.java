/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FWDTicTacToe.GameRules;
import FWDTicTacToe.GameTools.IBoard;
/**
 *
 * @author Vegi
 * @param <TIdentifier>
 * @param <TGameResult>
 */
public interface IBoardGameRule<TIdentifier, TGameResult> {
    int GetPlayerNum();
    IBoard<TIdentifier> GetBoard();
    void InitGame();
    TGameResult GetGameResult(TIdentifier id, int row, int col);
    boolean PlayerMove(TIdentifier id, int rowId, int colId);
}
