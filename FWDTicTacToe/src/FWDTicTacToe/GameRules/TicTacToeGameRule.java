/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FWDTicTacToe.GameRules;

import FWDTicTacToe.Enums.GameResult;
import FWDTicTacToe.Enums.TicTacToeIdentifier;
import FWDTicTacToe.GameTools.IBoard;
import FWDTicTacToe.GameTools.TicTacToeBoard;

/**
 *
 * @author Vegi
 */
public class TicTacToeGameRule implements IBoardGameRule<TicTacToeIdentifier, GameResult> {
    private final IBoard<TicTacToeIdentifier> _board;
    private final TicTacToeIdentifier[][] _boardDetail;
    private final int _playerCount = 2;
    
    public TicTacToeGameRule(int boardRowCount) {
        _board = new TicTacToeBoard(boardRowCount);
        _boardDetail = _board.GetBoardDetail();
    }
    
    @Override
    public int GetPlayerNum() {
        return _playerCount;
    }

    @Override
    public IBoard<TicTacToeIdentifier> GetBoard() {
        return _board;
    }

    @Override
    public void InitGame() {
        for (int row = 0; row < _boardDetail.length; ++row) {
            for (int col = 0; col < _boardDetail[row].length; ++col) {
                _boardDetail[row][col] = TicTacToeIdentifier.EMPTY;
            }
        }
    }

    @Override
    public GameResult GetGameResult(TicTacToeIdentifier id, int row, int col) {
        if (HasWon(id, row, col))
            return GameResult.WIN;
        else if (IsDraw())
            return GameResult.DRAW;
        
        return GameResult.EMPTY;
    }

    @Override
    public boolean PlayerMove(TicTacToeIdentifier id, int rowId, int colId) {
        if (rowId >= 0 && rowId < _boardDetail.length && colId >= 0 && colId < _boardDetail.length && _boardDetail[rowId][colId] == TicTacToeIdentifier.EMPTY) {
            int currentRowId = rowId;
            int currentColId = colId;
            _boardDetail[currentRowId][currentColId] = id;
            return true;
        }
        
        return false;
    }
    
    private boolean HasWon(TicTacToeIdentifier id, int rowId, int colId) {
        boolean isSame = true;
        
        for (int row = 0; row < _boardDetail.length && isSame; row++)
            isSame = _boardDetail[row][colId].equals(id);
        
        if (isSame) return true;
        
        isSame = true;
        
        for (int col = 0; col < _boardDetail[rowId].length && isSame; col++)
            isSame = _boardDetail[rowId][col].equals(id);
        
        if (isSame) return true;
        
        isSame = true;
        
        for (int row = 0; row < _boardDetail.length && isSame; row++)
            isSame = _boardDetail[row][row].equals(id);
        
        return isSame;
    }
    
    private boolean IsDraw() {
        for (TicTacToeIdentifier[] _boardDetail1 : _boardDetail) {
            for (TicTacToeIdentifier _boardDetail11 : _boardDetail1) {
                if (_boardDetail11 == TicTacToeIdentifier.EMPTY) {
                    return false;
                }
            }
        }
      
        return true;
    }
}
