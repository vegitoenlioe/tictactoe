/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FWDTicTacToe.GamePlayers;

import FWDTicTacToe.Enums.GameResult;
import FWDTicTacToe.Enums.TicTacToeIdentifier;

/**
 *
 * @author Vegi
 */
public class TicTacToePlayer implements IPlayer<TicTacToeIdentifier, GameResult> {
    private final String _name;
    private TicTacToeIdentifier _id;
    private GameResult _result;
    
    public TicTacToePlayer(String name) {
        _name = name;
    }
    
    @Override
    public String GetName() {
        return _name;
    }

    @Override
    public boolean SetIdentifier(TicTacToeIdentifier id) {
        _id = id;
        
        return true;
    }

    @Override
    public TicTacToeIdentifier GetIdentifier() {
        return _id;
    }

    @Override
    public boolean SetGameResult(GameResult result) {
        _result = result;
        
        return true;
    }

    @Override
    public GameResult GetGameResult() {
        return _result;
    }
}
