/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FWDTicTacToe.GamePlayers;

/**
 *
 * @author Vegi
 * @param <TIdentifier>
 * @param <TGameResult>
 */
public interface IPlayer<TIdentifier, TGameResult> {
    String GetName();
    boolean SetIdentifier(TIdentifier id);
    TIdentifier GetIdentifier();
    boolean SetGameResult(TGameResult result);
    TGameResult GetGameResult();
}
