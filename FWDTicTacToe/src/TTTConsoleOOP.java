import FWDTicTacToe.Enums.GameResult;
import FWDTicTacToe.Enums.GameStatus;
import FWDTicTacToe.Enums.TicTacToeIdentifier;
import FWDTicTacToe.GameComposer.IBoardGame;
import FWDTicTacToe.GameComposer.TicTacToeGame;
import FWDTicTacToe.GamePlayers.IPlayer;
import FWDTicTacToe.GameRules.IBoardGameRule;
import FWDTicTacToe.GameTools.IBoard;
import java.util.List;
import java.util.Scanner;
/**
 * Tic-Tac-Toe: Two-player console, non-graphics, non-OO version.
 * All variables/methods are declared as static (belong to the class)
 *  in the non-OO version.
 */
public class TTTConsoleOOP {
    private static Integer _rows;
    private static GameStatus _currentState;
    private static IPlayer<TicTacToeIdentifier, GameResult> _currentPlayer;
    private static int _currentRow, _currentCol;
    private static Scanner _in = new Scanner(System.in);
 
    /** The entry main method (the program starts here)
     * @param args */
    public static void main(String[] args) {
        boolean sizeAchieved = false;
        boolean processStopped = false;
        do {
            System.out.print("Insert board size: ");
            try{
                _rows = _in.nextInt();
                
                if (_rows <= 0)
                    System.out.println("board size must be bigger than 0.");
                else
                    sizeAchieved = true;
            }
            catch(Exception e) {
                System.out.println("board size must be integer.");
                processStopped = true;
            }
        } while (!sizeAchieved && !processStopped);
        
        if(sizeAchieved) {
            IBoardGame<TicTacToeIdentifier, GameResult, GameStatus> boardGame = new TicTacToeGame(_rows);
            IBoardGameRule<TicTacToeIdentifier, GameResult> rule = boardGame.GetGameRule();
            List<IPlayer<TicTacToeIdentifier, GameResult>> players = boardGame.GetPlayers();
            IBoard<TicTacToeIdentifier> board = rule.GetBoard();
            rule.InitGame();
            int playerIdx = 0;
            _currentPlayer = players.get(playerIdx);
            // Play the game once
            boardGame.Start();
            do {
                PlayerMove(_currentPlayer, rule);

                players.forEach((player) -> {
                    boardGame.UpdateGame(player.GetIdentifier(), _currentRow, _currentCol);
                });

                _currentState = boardGame.GetGameStatus();

                PrintBoard(board);

                if(++playerIdx == players.size())
                    playerIdx = 0;

                _currentPlayer = players.get(playerIdx);
            } while (_currentState == GameStatus.STARTED);
        }
    }
 
    /**
     * @param player
     * @param rule */
    public static void PlayerMove(IPlayer<TicTacToeIdentifier, GameResult> player, IBoardGameRule<TicTacToeIdentifier, GameResult> rule) {
        boolean validInput = false;  // for input validation
        do {
            if (player.GetIdentifier().equals(TicTacToeIdentifier.CROSS)) {
                System.out.printf("Player 'X', enter your move (row[1-%d] column[1-%d]): ", _rows, _rows);
            } else {
                System.out.printf("Player 'O', enter your move (row[1-%d] column[1-%d]): ", _rows, _rows);
            }
            int row = _in.nextInt() - 1;  // array index starts at 0 instead of 1
            int col = _in.nextInt() - 1;
            if (rule.PlayerMove(player.GetIdentifier(), row, col)) {
                _currentRow = row;
                _currentCol = col;
                validInput = true;
            } else {
                System.out.println("This move at (" + (row + 1) + "," + (col + 1)
                     + ") is not valid. Try again...");
            }
        } while (!validInput);  // repeat until input is valid
    }
 
    /** Print the game board
     * @param board */
    public static void PrintBoard(IBoard<TicTacToeIdentifier> board) {
        TicTacToeIdentifier[][] boardDetail = board.GetBoardDetail();
        for (int row = 0; row < boardDetail.length; ++row) {
            for (int col = 0; col < boardDetail.length; ++col) {
                PrintCell(boardDetail[row][col]); // print each of the cells
                if (col != boardDetail.length - 1) {
                    System.out.print("|");   // print vertical partition
                }
            }
            System.out.println();
            if (row < boardDetail.length - 1) {
                String line = "";
                
                for(int i = 0; i < _rows; i++)
                    line += "----";
                
                System.out.println(line);
            }
        }
        System.out.println();
    }
 
    /** Print a cell with the specified "content"
      * @param content */
    public static void PrintCell(TicTacToeIdentifier content) {
        switch (content) {
            case EMPTY:  System.out.print("   "); break;
            case NOUGHT: System.out.print(" O "); break;
            case CROSS:  System.out.print(" X "); break;
        }
    }
}