/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FWDTicTacToe.Services;

/**
 *
 * @author Vegi
 */
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import FWDTicTacToe.Enums.GameResult;
import FWDTicTacToe.Enums.GameStatus;
import FWDTicTacToe.Enums.TicTacToeIdentifier;
import FWDTicTacToe.GameComposer.IBoardGame;
import FWDTicTacToe.GameComposer.TicTacToeGame;
import FWDTicTacToe.GamePlayers.IPlayer;
import FWDTicTacToe.GameRules.IBoardGameRule;
import FWDTicTacToe.GameTools.IBoard;
import java.util.List;
import java.util.Scanner;
import javax.servlet.annotation.WebServlet;

@WebServlet("/tictactoe")
public class PlayGame extends HttpServlet {
    private static Integer _rows;
    private static GameStatus _currentState;
    private static IPlayer<TicTacToeIdentifier, GameResult> _currentPlayer;
    private static int _currentRow, _currentCol;
    private static Scanner _in = new Scanner(System.in);
    
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        response.setContentType("text/html; charset=UTF-8");
        try (
                PrintWriter out = response.getWriter()
          ) {
            String size = request.getParameter("size");
            _rows = Integer.parseInt(size);

            IBoardGame<TicTacToeIdentifier, GameResult, GameStatus> boardGame = new TicTacToeGame(_rows);
            IBoardGameRule<TicTacToeIdentifier, GameResult> rule = boardGame.GetGameRule();
            List<IPlayer<TicTacToeIdentifier, GameResult>> players = boardGame.GetPlayers();
            IBoard<TicTacToeIdentifier> board = rule.GetBoard();
            rule.InitGame();
            int playerIdx = 0;
            _currentPlayer = players.get(playerIdx);
            
            if(_currentState != GameStatus.STARTED)
                boardGame.Start();
            
            if(_currentState == GameStatus.STARTED) {
                PlayerMove(_currentPlayer, rule, request);

                players.forEach((player) -> {
                    boardGame.UpdateGame(player.GetIdentifier(), _currentRow, _currentCol);
                });

                _currentState = boardGame.GetGameStatus();

                PrintBoard(board, out);

                if(++playerIdx == players.size())
                    playerIdx = 0;

                _currentPlayer = players.get(playerIdx);
            };
            
            request.getRequestDispatcher("WEB-INF/tictactoe.jsp").forward(request, response);
        }
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
                throws IOException, ServletException {
        doGet(request, response);
    }

    private static String HtmlFilter(String message) {
        if (message == null) return null;
        int len = message.length();
        StringBuilder result = new StringBuilder(len + 20);
        char aChar;

        for (int i = 0; i < len; ++i) {
            aChar = message.charAt(i);
            switch (aChar) {
                case '<': result.append("&lt;"); break;
                case '>': result.append("&gt;"); break;
                case '&': result.append("&amp;"); break;
                case '"': result.append("&quot;"); break;
                default: result.append(aChar);
            }
        }
        return (result.toString());
    }
    
    private static void PlayerMove(IPlayer<TicTacToeIdentifier, GameResult> player, IBoardGameRule<TicTacToeIdentifier, GameResult> rule, HttpServletRequest request) {
        boolean validInput = false;
        //do {
            if (player.GetIdentifier().equals(TicTacToeIdentifier.CROSS)) {
                request.setAttribute("playerId", "X");
            } else {
                request.setAttribute("playerId", "O");
            }
            
            request.setAttribute("rowCount", _rows);
            
            int row = _in.nextInt() - 1;  // array index starts at 0 instead of 1
            int col = _in.nextInt() - 1;
            if (rule.PlayerMove(player.GetIdentifier(), row, col)) {
                _currentRow = row;
                _currentCol = col;
                validInput = true;
            } else {
                /*out.println("This move at (" + (row + 1) + "," + (col + 1)
                     + ") is not valid. Try again...");*/
            }
        //} while (!validInput);  // repeat until input is valid
    }

    private static void PrintBoard(IBoard<TicTacToeIdentifier> board, PrintWriter out) {
        TicTacToeIdentifier[][] boardDetail = board.GetBoardDetail();
        for (int row = 0; row < boardDetail.length; ++row) {
            for (int col = 0; col < boardDetail.length; ++col) {
                PrintCell(boardDetail[row][col], out); // print each of the cells
                if (col != boardDetail.length - 1) {
                    out.print("|");   // print vertical partition
                }
            }
            out.println();
            if (row < boardDetail.length - 1) {
                String line = "";
                
                for(int i = 0; i < _rows; i++)
                    line += "----";
                
                out.println(line);
            }
        }
        out.println();
    }

    public static void PrintCell(TicTacToeIdentifier content, PrintWriter out) {
        switch (content) {
            case EMPTY:  out.print("   "); break;
            case NOUGHT: out.print(" O "); break;
            case CROSS:  out.print(" X "); break;
        }
    }
}
