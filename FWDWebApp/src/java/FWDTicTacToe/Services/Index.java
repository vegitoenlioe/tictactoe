/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FWDTicTacToe.Services;

/**
 *
 * @author Vegi
 */
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class Index extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        response.setContentType("text/html; charset=UTF-8");
        try (
                PrintWriter out = response.getWriter()
          ) {
            String size = request.getParameter("size");
            if (size == null || (size = HtmlFilter(size.trim())).length() == 0) {
                out.println("Size: MISSING");
                out.println("<a href='index.html'>BACK</a>");
            } else {
                RequestDispatcher view = request.getRequestDispatcher("WEB-INF/tictactoe.jsp");
                request.setAttribute("size", size);
                view.forward(request, response);
            }
        }
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
                throws IOException, ServletException {
        doGet(request, response);
    }

    private static String HtmlFilter(String message) {
        if (message == null) return null;
        int len = message.length();
        StringBuilder result = new StringBuilder(len + 20);
        char aChar;

        for (int i = 0; i < len; ++i) {
            aChar = message.charAt(i);
            switch (aChar) {
                case '<': result.append("&lt;"); break;
                case '>': result.append("&gt;"); break;
                case '&': result.append("&amp;"); break;
                case '"': result.append("&quot;"); break;
                default: result.append(aChar);
            }
        }
        return (result.toString());
    }
}