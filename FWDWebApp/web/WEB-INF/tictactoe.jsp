<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>TicTacToe Game</title>
    </head>
    <body>
        <form method="get" action="setPosition">
            <input type="submit" value="PROCEED"/>
            <input type="hidden" name="size" value="${size}"/>
        </form>
        <table style="width: 100%;">
            <th colspan="4"></th>
            <tr>
                <td style="width: 25%;">
                    Player ${playerId}, enter your move (row[1-${rowCount}] and column[1-${rowCount}])
                </td>
                <td>:</td>
                <td style="width: 12%;">
                    <input type="number" id="rowInput" />
                </td>
                <td>
                    <input type="number" id="colInput" />
                </td>
            </tr>
            <tr>

            </tr>
        </table>
    </body>
</html>
